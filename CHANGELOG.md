# Changelog

## 0.2.0 - 2023-09-05

### Added

  - Add `pre-commit` hook

## 0.1.0 - 2023-09-05

*Initial release*
