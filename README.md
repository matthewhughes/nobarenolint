# nobarenolint

`nobarenolint` checks for bare
[`//nolint`](https://golangci-lint.run/usage/false-positives/#nolint-directive)
directives in your code. Usage:

    $ nobarenolint <filename>

This repo also provides a [`pre-commit`](https://pre-commit.com/) hook which cam
be configured like:

``` yaml
-   repo: https://gitlab.com/matthewhughes/nobarenolint
    rev: v0.2.0
    hooks:
    -   id: nobarenolint
```

## Motivation

These directives should always include the names of the linters being ignored,
e.g. `//nolint:golint,unused` in order to:

  - More clearly communicate intent behind the exclusion
  - Help identify unnecessary directives, e.g. if a directive is for a linter
    that is no longer run

Compare:

``` go
func helloHandler(w http.ResponseWriter, _ *http.Request) {
	io.WriteString(w, "Hello from a HandleFunc #2!\n") //nolint
}
```

With:

``` go
func helloHandler(w http.ResponseWriter, _ *http.Request) {
	io.WriteString(w, "Hello from a HandleFunc #2!\n") //nolint:errcheck
}

```

The latter helps, at a glance, to suggest the exception has *something* to do
with error checking, and gives anyone curious some more information they can
easily search to find out more.
