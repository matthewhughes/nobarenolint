package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
)

func TestPassesOnValidFiles(t *testing.T) {
	tempDir := t.TempDir()

	for i, tc := range []struct {
		desc        string
		fileContent string
	}{
		{
			"file with not comments",
			dedent.Dedent(`package main

			func main() {
			}
			`),
		},
		{
			"file with valid nolint comment",
			dedent.Dedent(`package main

			func main() { //nolint:errcheck
			}
			`),
		},
		{
			"file with multiple valid nolint comments",
			dedent.Dedent(`package main

			func main() { //nolint:errcheck,staticcheck
			}
			`),
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			fname := filepath.Join(tempDir, fmt.Sprintf("file%d.go", i))
			require.NoError(t, os.WriteFile(fname, []byte(tc.fileContent), 0o600))

			require.Equal(t, 0, run([]string{"prog-name", fname}))
		})
	}
}

func TestFailsOnInvalidFiles(t *testing.T) {
	tempDir := t.TempDir()

	for i, tc := range []struct {
		fileContent      string
		expectedBadLines []int
	}{
		{
			dedent.Dedent(`package main

			func main() { //nolint
			}
			`),
			[]int{3},
		},
		{
			dedent.Dedent(`package main

			func main() { // nolint
			}
			`),
			[]int{3},
		},
		{
			dedent.Dedent(`package main

			func main() { //nolint // some extra comment
			}
			`),
			[]int{3},
		},
		{
			dedent.Dedent(`package main

			func main() { //nolint some unrelated stuff
			}
			`),
			[]int{3},
		},
		{
			dedent.Dedent(`package main

			func main() { //nolint
			}

			func foo() { //nolint
			}
			`),
			[]int{3, 6},
		},
	} {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			fname := filepath.Join(tempDir, fmt.Sprintf("file%d.go", i))
			var expectedErrLines []string
			for _, n := range tc.expectedBadLines {
				expectedErrLines = append(
					expectedErrLines,
					fmt.Sprintf("%s: Bare //nolint at line %d", fname, n),
				)
			}
			require.NoError(t, os.WriteFile(fname, []byte(tc.fileContent), 0o600))

			retCode, gotOut, gotErr := runWithCapture(t, []string{"prog-name", fname})

			gotErrLines := strings.Split(string(gotErr), "\n")
			require.Equal(t, retCode, 1)
			require.Equal(t, "", string(gotOut))
			require.Equal(t, expectedErrLines, gotErrLines[:len(gotErrLines)-1])
		})
	}
}

func TestFailsOnUnredableFile(t *testing.T) {
	fname := filepath.Join(t.TempDir(), "file.go")
	fileContent := "NOT VALID GO\n"
	expectedErrPrefix := "Failed to parse file " + fname + ":"
	require.NoError(t, os.WriteFile(fname, []byte(fileContent), 0o600))

	retCode, gotOut, gotErr := runWithCapture(t, []string{"prog-name", fname})

	require.Equal(t, 1, retCode)
	require.Equal(t, "", string(gotOut))
	require.Contains(t, string(gotErr), expectedErrPrefix)
}

func TestContinuesOnUnreadableFile(t *testing.T) {
	unreadableFname := filepath.Join(t.TempDir(), "unreadable.go")
	unreadableContent := "NOT VALID GO\n"
	require.NoError(t, os.WriteFile(unreadableFname, []byte(unreadableContent), 0o600))

	failingFname := filepath.Join(t.TempDir(), "bad.go")
	failingContent := dedent.Dedent(`package main

	func main () { //nolint
	}
	`)
	failingError := failingFname + ": Bare //nolint at line 3"
	require.NoError(t, os.WriteFile(failingFname, []byte(failingContent), 0o600))

	retCode, gotOut, gotErr := runWithCapture(
		t,
		[]string{"prog-name", unreadableFname, failingFname},
	)

	require.Equal(t, 1, retCode)
	require.Equal(t, "", string(gotOut))
	require.Contains(t, string(gotErr), failingError)
}

func runWithCapture(t *testing.T, args []string) (int, []byte, []byte) {
	t.Helper()
	stdoutr, stdoutw, err := os.Pipe()
	require.NoError(t, err)

	stderrr, stderrw, err := os.Pipe()
	require.NoError(t, err)

	oldOut := os.Stdout
	oldErr := os.Stderr
	os.Stdout = stdoutw
	os.Stderr = stderrw
	t.Cleanup(func() {
		os.Stdout = oldOut
		os.Stderr = oldErr
	})

	returnCode := run(args)
	stderrw.Close()
	stdoutw.Close()

	capturedOut, err := io.ReadAll(stdoutr)
	require.NoError(t, err)
	capturedErr, err := io.ReadAll(stderrr)
	require.NoError(t, err)

	return returnCode, capturedOut, capturedErr
}
